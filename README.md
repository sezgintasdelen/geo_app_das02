# GEO_APP_DAS02

Jupyter Notebook for locating an induced micro-seismic event (Mw<2.0) in a geothermal field, using DAS recordings

Part 1: measuring P-wave arrival times
